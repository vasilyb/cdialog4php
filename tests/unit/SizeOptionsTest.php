<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\Size;
use \CDialog4PHP\UnitTests\Options\SizeOptionsImpl;
use \CDialog4PHP\UnitTests\Aux\{OptionsTester, PropertiesTester};

class SizeOptionsTest extends \Codeception\Test\Unit
{
    // tests
    public function testDefaultHeight()
    {
        $size_options = new SizeOptionsImpl;
        self::assertEquals(Size::MINIMUM,
            $size_options->getHeight());
    }

    public function testHeight()
    {
        $data = array();

        $data['Object'] = new SizeOptionsImpl;
        $data['Property'] = 'Height';
        $data['Expectation'] = 1;

        (new PropertiesTester)->testProperty($data);
    }

    public function testDefaultWidth()
    {
        $size_options = new SizeOptionsImpl;
        self::assertEquals(Size::MINIMUM, $size_options->getWidth());
    }

    public function testWidth()
    {
        $data = array();

        $data['Object'] = new SizeOptionsImpl;
        $data['Property'] = 'Width';
        $data['Expectation'] = 2;

        (new PropertiesTester)->testProperty($data);
    }

    public function testSize()
    {
        $size_options = new SizeOptionsImpl;
        $size_options->setHeight(20);
        $size_options->setWidth(30);
        self::assertEquals('20 30', $size_options->getSize());
    }

    public function testDefaultHeightSetter()
    {
        $size_options = new SizeOptionsImpl;
        $size_options->setHeight(10);
        $size_options->setHeight();
        self::assertEquals(Size::MINIMUM,
            $size_options->getHeight());
    }

    public function testDefaultWidthSetter()
    {
        $size_options = new SizeOptionsImpl;
        $size_options->setWidth(30);
        $size_options->setWidth();
        self::assertEquals(Size::MINIMUM,
            $size_options->getWidth());
    }
}
