<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;

class EntriesTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreateEntries()
    {
        $entries = new \CDialog4PHP\Entries;
        self::assertNotNull($entries);
    }

    public function testAppendEntry()
    {
        $entries = new \CDialog4PHP\Entries;

        $entry = new \CDialog4PHP\Entry;
        $entries->append($entry);
        self::assertEquals(1, $entries->count(),
            "Entries must allow to append Entry");

        $not_entry = new \CDialog4PHP\BuildListOptions;
        $entries->append($not_entry);
        self::assertEquals(1, $entries->count(),
            "Entries mustn't allow to append() anyting except Entry");
    }
}
