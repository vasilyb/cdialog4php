<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\{
    BuildListOptions, Config, Entry, EntryState, Size};
use \CDialog4PHP\UnitTests\SizeOptionsTester;
use \CDialog4PHP\UnitTests\Aux\{
    OptionsTester, ParentClassTester, PropertiesTester};
use \Codeception\Test\Unit;
use \Codeception\Util\Debug;

class BuildListOptionsTest extends Unit
{
    use ParentClassTester;
    use SizeOptionsTester;

    // tests
    public function testText()
    {
        $data = array();

        $data['Object'] = new BuildListOptions;
        $data['Property'] = 'Text';
        $data['Expectation'] = 'CDialog4PHP';

        (new PropertiesTester)->testProperty($data);
    }

    public function testListHeight()
    {
        $data = array();

        $data['Object'] = new BuildListOptions;
        $data['Property'] = 'ListHeight';
        $data['Expectation'] = 15;

        (new PropertiesTester)->testProperty($data);
    }

    public function testDefaultListHeight()
    {
        $options = new BuildListOptions;
        self::assertEquals(Size::MINIMUM, $options->getListHeight());
    }

    public function testDefaultListHeightSetter()
    {
        $options = new BuildListOptions;
        $options->setListHeight(9);
        $options->setListHeight();
        self::assertEquals(Size::MINIMUM, $options->getListHeight());
    }

    private function createOptions()
    {
        $options = new BuildListOptions;
        $options->setHeight(Size::SCREEN);
        $options->setWidth(Size::SCREEN);
        return $options;
    }

    public function testOptions()
    {
        $data = array();

        $data['Options'] = self::createOptions();
        $data['Arguments'] = array('text'=>'Text', 'listHeight'=>10);
        $data['Expectation'] =
            "--buildlist 'Text' -1 -1 10 fbsd FreeBSD on";

        (new OptionsTester)->testAllOptions($data);
    }

    public function testAncestor()
    {
        self::testParentClass('CDialog4PHP\BoxOptions',
            new BuildListOptions);
    }

    public function testSizeOptionsTraitUsage()
    {
        self::testSizeOptionsMethodsContained(new BuildListOptions);
    }

    private static $findForTag;
    private static function findByTag(Entry $entry)
    {
        return BuildListOptionsTest::$findForTag === $entry->getTag();
    }

    public function testEntries()
    {
        $options = new BuildListOptions();

        $options->addEntry('ghb', 'GhostBSD', EntryState::OFF);
        $options->addEntry('tob', 'True OS', EntryState::OFF);
        $options->addEntry('frb', 'FreeBSD', EntryState::OFF);
        $options->addEntry('all', 'Alpine', EntryState::OFF);
        $options->addEntry('mit', 'MINIX', EntryState::ON);
        $options->addEntry('dil', 'Debian', EntryState::ON);

        $expected_entries = array(
            array('tag'=>'ghb', 'item'=>'GhostBSD', 'state'=>'off'),
            array('tag'=>'tob', 'item'=>'True OS', 'state'=>'off'),
            array('tag'=>'frb', 'item'=>'FreeBSD', 'state'=>'off'),
            array('tag'=>'all', 'item'=>'Alpine', 'state'=>'off'),
            array('tag'=>'mit', 'item'=>'MINIX', 'state'=>'on'),
            array('tag'=>'dil', 'item'=>'Debian', 'state'=>'on')
        );

        $actual_entries = $options->getEntries();

        self::assertEquals(
            count($expected_entries), count($actual_entries));

        $filter = array(__CLASS__, 'findByTag');
        foreach ($expected_entries as $entry):
            BuildListOptionsTest::$findForTag = $entry['tag'];
            $found_entries = array_filter($actual_entries, $filter);
            self::assertEquals(1, count($found_entries));

            $first_key = array_keys($found_entries)[0];
            $found = $found_entries[$first_key];
            self::assertEquals($entry['item'], $found->getItem());
            self::assertEquals($entry['state'], $found->getState());
        endforeach;
        unset($entry);
    }
}
?>
