<?php
declare(strict_types=1);

namespace CDialog4PHP\UnitTests;

use \CDialog4PHP\Entry;
use \CDialog4PHP\EntryState;
use \CDialog4PHP\UnitTests\Aux\PropertiesTester;

class EntryTest extends \Codeception\Test\Unit
{
    private $propTester;
    private $entry;

    public function _before()
    {
        $this->propTester = new PropertiesTester;
        $this->entry = new Entry;
    }

    public function testTag()
    {
        $data = array(
            'Object'=>$this->entry,
            'Property'=>'Tag',
            'Expectation'=>'tag'
        );
        $this->propTester->testProperty($data);
    }

    public function testItem()
    {
        $data = array(
            'Object'=>$this->entry,
            'Property'=>'Item',
            'Expectation'=>'item'
        );
        $this->propTester->testProperty($data);
    }

    public function testState()
    {
        $data = array(
            'Object'=>$this->entry,
            'Property'=>'State',
            'Expectation'=>EntryState::ON
        );
        $this->propTester->testProperty($data);
    }

    public function testStateCorrectness()
    {
        $caugth = FALSE;

        try {
            $this->entry->setState('Only "on" and "off" are possible');
        } catch (\Exception $e) {
            self::assertEquals(
                'CDialog4PHP\Entry::setState() - $state must be "off" or "on".',
                $e->getMessage());
            $caught = TRUE;
        }

        self::assertTrue($caught);
    }
}
?>
