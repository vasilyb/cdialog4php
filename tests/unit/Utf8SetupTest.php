<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\UnitTests\Aux\Env;

class Utf8SetupTest extends \Codeception\Test\Unit
{
    private $env;

    protected function _before()
    {
        \Codeception\Util\Debug::debug(''); // beautify output
        $this->env = new Env;
    }

    protected function _after()
    {
    }

    // tests
    public function testSetAndUnsetNoUtf8AcsIfUtf8()
    {
        $collation = getenv(COLLATION);
        $no_acs = getenv(NO_ACS);
        \Codeception\Util\Debug::debug("Initial collation: '$collation'.");
        \Codeception\Util\Debug::debug("Initial flag: '$no_acs'.");

        $this->env->setCollation('en_US.UTF-8'); // enforce UTF-8 for the case

        $collation = getenv(COLLATION);
        $this->env->resetNoAcs();
        \Codeception\Util\Debug::debug("New collation: '$collation'.");
        \Codeception\Util\Debug::debug("New flag: '$no_acs'.");

        $utf_setup = new \CDialog4PHP\Utf8Setup;
        $utf_setup->setUp();

        $no_acs = getenv(NO_ACS);
        \Codeception\Util\Debug::debug("Actual flag (after setup): '$no_acs'.");
        $this->assertEquals($no_acs, '1');

        $utf_setup->setDown();

        $no_acs = getenv(NO_ACS);
        \Codeception\Util\Debug::debug("Actual flag (after setdown): '$no_acs'.");
        $this->assertEquals($no_acs, '');
    }

    public function testNotSetAndNotUnsetUtf8AcsIfNotUtf8()
    {
        $this->env->setCollation('ru_RU.KOI8-R'); // enforce not UTF-8 for the case
        $this->env->resetNoAcs();

        $utf_setup = new \CDialog4PHP\Utf8Setup;

        $utf_setup->setUp();
        $no_acs = getenv(NO_ACS);
        $this->assertEquals($no_acs, '');

        $utf_setup->setDown();
        $no_acs = getenv(NO_ACS);
        $this->assertEquals($no_acs, '');
    }

    public function testUnsetAndSetUtf8AcsIfNotUtf8()
    {
        $this->env->setCollation('ru_RU.KOI8-R'); // enforce not UTF-8 for the case
        $this->env->setNoAcs('1');

        $utf_setup = new \CDialog4PHP\Utf8Setup;

        $utf_setup->setUp();
        $no_acs = getenv(NO_ACS);
        $this->assertEquals($no_acs, '');

        $utf_setup->setDown();
        $no_acs = getenv(NO_ACS);
        $this->assertEquals($no_acs, '1');
    }
}
