<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\{CommonOptions, Config};
use \CDialog4PHP\UnitTests\Options\BoxOptionsImpl;
use \CDialog4PHP\UnitTests\Aux\ParentClassTester;

class ConfigTest extends \Codeception\Test\Unit
{
    use ParentClassTester;

    // tests
    public function testImplodeOptions()
    {
        $common = new CommonOptions;
        $common->setTitle('Common');
        $box = new BoxOptionsImpl;
        $configuration = new Config($common, $box);
        $expected = $common->getOptions() . ' ' . $box->getOptions();
        $actual = $configuration->getOptions();
        self::assertEquals($expected, $actual);
    }

    public function testEscapeOptions()
    {
        $common = new CommonOptions;
        $common->setTitle("It's mine");
        $box = new BoxOptionsImpl;
        $configuration = new Config($common, $box);
        $expected = "--title 'It'\\''s mine' --version";
        $actual = $configuration->getOptions();
        self::assertEquals($expected, $actual);
    }

    public function testAncestor()
    {
        $common = new CommonOptions;
        $box = new BoxOptionsImpl;
        $configuration = new Config($common, $box);
        self::testParentClass('CDialog4PHP\Options',
            $configuration);
    }

    public function testCommonOptionsEdits()
    {
        $common = new CommonOptions;
        $box = new BoxOptionsImpl;
        $configuration = new Config($common, $box);
        $configuration->editCommonOptions()->setTitle('Edited');
        $expected = "--title 'Edited' --version";
        $actual = $configuration->getOptions();
        self::assertEquals($expected, $actual,
            'Edit common options: set title');
    }

    public function testBoxOptionsEdits()
    {
        $common = new CommonOptions;
        $box = new BoxOptionsImpl;
        $configuration = new Config($common, $box);
        $configuration->editBoxOptions()->setHello('from Vasily');
        $expected = "--title '' --version --and-hello 'from Vasily'";
        $actual = $configuration->getOptions();
        self::assertEquals($expected, $actual,
            'Edit box options: set hello');
    }
}

