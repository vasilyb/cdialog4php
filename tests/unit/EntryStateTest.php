<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;

use \CDialog4PHP\EntryState;

class EntryStateTest extends \Codeception\Test\Unit
{
    // tests
    public function testOffState()
    {
        self::assertEquals('off', EntryState::OFF);
    }

    public function testOnState()
    {
        self::assertEquals('on', EntryState::ON);
    }
}
