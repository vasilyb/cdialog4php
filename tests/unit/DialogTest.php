<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\{CommonOptions, Config, Dialog};
use \CDialog4PHP\UnitTests\Aux\Env;
use \CDialog4PHP\UnitTests\Options\BoxOptionsImpl;

class BoxOptionsExtendedImpl extends BoxOptionsImpl
{
    private $opened_acs = '';

    public function getOpenedAcs(): string
    {
        return $this->opened_acs;
    }

    /* Trick to get env vars during performOpen() */
    protected function tillDialogOpen(): string
    {
        $this->opened_acs = getenv(NO_ACS);
        return '';
    }

    public function getOptions(): string
    {
        $this->tillDialogOpen();
        return parent::getOptions();
    }
}

class DialogImpl extends Dialog
{
    private $box_options;

    public function getOpenedAcs(): string
    {
        return $this->box_options->getOpenedAcs();
    }

    public function __construct()
    {
        $common_options = new CommonOptions;
        $this->box_options = new BoxOptionsExtendedImpl;
        $config = new Config($common_options, $this->box_options);
        parent::__construct($config);
    }
}

class DialogTest extends \Codeception\Test\Unit
{
    private $dialog;
    private $env;

    protected function _before()
    {
        $this->dialog = new DialogImpl();
        $this->env = new Env;

        \Codeception\Util\Debug::debug('');
    }

    protected function _after()
    {
    }

    // tests
    public function testUtf8SetupCallDuringOpenCall()
    {
        $this->env->setCollation('en_US.UTF-8');
        $this->env->resetNoAcs();

        $this->dialog->open();

        \Codeception\Util\Debug::debug("Defined COLLATION: " . COLLATION);
        \Codeception\Util\Debug::debug("Defined NO_ACS: " . NO_ACS);

        $lc_all = getenv(COLLATION);
        $op_acs = $this->dialog->getOpenedAcs();
        $no_acs = getenv(NO_ACS);

        $this->assertEquals('en_US.UTF-8', $lc_all);
        $this->assertEquals('1', $op_acs); // when dialog is in open state
        $this->assertEquals('', $no_acs); // because flag must be resetted
    }
}
