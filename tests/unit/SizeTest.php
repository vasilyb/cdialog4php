<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;

use \CDialog4PHP\Size;

class SizeTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testScreenSize()
    {
        $size = Size::SCREEN;
        self::assertEquals(-1, $size, 'Match parent test failed');
    }
    
    public function testMinumumSize()
    {
        $size = Size::MINIMUM;
        self::assertEquals(0, $size, 'Wrap content test failed');
    }
}
