<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use CDialog4PHP\UnitTests\Aux\ClassMethodsTester;

trait SizeOptionsTester
{
    use ClassMethodsTester;

    private function testSizeOptionsMethodsContained($object)
    {
        self::testClassContainsMethods($object,
            'setHeight', 'getHeight', 'setWidth', 'getWidth');
    }
}
?>
