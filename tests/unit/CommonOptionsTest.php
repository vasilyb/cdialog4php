<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\{CommonOptions, Config};
use \CDialog4PHP\UnitTests\Aux\OptionsTester;
use \CDialog4PHP\UnitTests\Aux\PropertiesTester;
use \CDialog4PHP\UnitTests\Options\BoxOptionsImpl;
use \Codeception\Test\Unit;

class CommonOptionsTest extends Unit
{
    // tests
    public function testTitle()
    {
        $data = array();

        $data['Object'] = new CommonOptions;
        $data['Property'] = 'Title';
        $data['Expectation'] = 'Cornerstone';

        (new PropertiesTester)->testProperty($data);
    }

    public function testOptions()
    {
        $data = array();

        $data['Options'] = new CommonOptions;
        $data['Arguments'] = array('title' => 'Dialog');
        $data['Expectation'] = "--title 'Dialog'";

        (new OptionsTester)->testAllOptions($data);
    }

    public function testEscape()
    {
        $data = array();

        $data['Options'] = new CommonOptions;
        $data['Arguments'] = array('title' => "That's all");
        $data['Expectation'] = "--title 'That'\''s all'";

        (new OptionsTester)->testAllOptions($data);
    }
}
?>

