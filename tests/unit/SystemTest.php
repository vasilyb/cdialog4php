<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;

class SystemTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCDialogInstalled()
    {
				$dialog = 'cdialog';
				$expected_exit_code = 0;
        passthru($dialog, $actual_exit_code);
				self::assertEquals($expected_exit_code, $actual_exit_code,
					"Exit code $actual_exit_code means a throuble running '$dialog'" . PHP_EOL .
					"Try to install it (FreeBSD example: 'sudo pkg install $dialog').");
    }
}
