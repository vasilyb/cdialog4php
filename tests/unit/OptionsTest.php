<?php
declare(strict_types=1);

namespace CDialog4PHP\UnitTests;
use \Codeception\Test\Unit as UnitTest;
use \CDialog4PHP\Options;
use \CDialog4PHP\UnitTests\Aux\OptionsTester;
use \CDialog4PHP\UnitTests\Options\OptionsImpl;

class OptionsTest extends UnitTest
{
    // tests
    public function testApostropheEscaping()
    {
        $data = array( );

        $data['Options'] = new OptionsImpl;
        $data['Expectation'] = "--an-option 'That'\''s all'";

        (new OptionsTester)->testAllOptions($data);
    }

    /* named form:    --option-name 'OPTION_VALUE'
     * nameless form: 'OPTION_VALUE'
     */
    public function testNamelessOption()
    {
        $options = new OptionsImpl;
        $options->setOptionName('');

        $data = array( 'Options' => $options,
            'Expectation' => "'That'\''s all'" );

        (new OptionsTester)->testAllOptions($data);
    }
}
?>
