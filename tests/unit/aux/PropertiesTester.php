<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;
use \PHPUnit\Framework\Assert;
use \CDialog4PHP\{Options, CommonOptions};
use \CDialog4PHP\UnitTests\Options\OptionsImpl;

// for unit testing of public set...() and get...() methods
class PropertiesTester
{
    private function testPropertyIsSetByDefault($options,
        string $property)
    {
        $get = "get$property";
        $value = $options->$get();
        $isset = isset($value);
        Assert::assertTrue($isset,
            "The '$property' property isn't set by default");
    }

    private function testPropertyGetAndSet($options,
        string $property, $expectation)
    {
        $get = "get$property";
        $set = "set$property";

        $options->$set($expectation);
        $actual = $options->$get();

        Assert::assertEquals($expectation, $actual,
            "The '$property' property setter/getter isn't working");
    }

    public function testProperty(array $data)
    {
        $options = ArrayContrib::getOrDefault('Object',
            $data, new OptionsImpl);
        $property = ArrayContrib::getOrDefault('Property',
            $data, 'Title');
        $expectation = ArrayContrib::getOrDefault(
            'Expectation', $data, '?');

        self::testPropertyIsSetByDefault($options, $property);
        self::testPropertyGetAndSet(
            $options, $property, $expectation);
    }
}
?>
