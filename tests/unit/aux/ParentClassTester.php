<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;
use \PHPUnit\Framework\Assert;

trait ParentClassTester
{
    private function testParentClass(string $expected,
        $classname_or_object)
    {
        $parent_classname = get_parent_class($classname_or_object);
        Assert::assertEquals($expected, $parent_classname);
    }
}
?>
