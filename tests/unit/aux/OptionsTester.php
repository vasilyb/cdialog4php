<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;
use \PHPUnit\Framework\Assert;
use \CDialog4PHP\{Options, CommonOptions};
use \CDialog4PHP\UnitTests\Aux\SetGetTester;
use \CDialog4PHP\UnitTests\Options\OptionsImpl;

class OptionsTester
{
    //private $setGetTester = new SetGetTester;

    public function testAllOptions(array $data)
    {
        $options = ArrayContrib::getOrDefault('Options',
            $data, new OptionsImpl);
        $arguments = ArrayContrib::getOrDefault('Arguments',
            $data, array());
        $expectation = ArrayContrib::getOrDefault(
            'Expectation', $data, '');

        foreach ($arguments as $parameter => $argument):
            $property = ucfirst($parameter);
            $set = "set$property";
            $options->$set($argument);
        endforeach;

        $actual = $options->getOptions();
        Assert::assertEquals($expectation, $actual);
    }
}
?>
