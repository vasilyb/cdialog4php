<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;

class ArrayContrib
{
    public static function getOrDefault($key, array $array,
        $default)
    {
        return array_key_exists($key, $array)?
            $array[$key]:
            $default;
    }
}
?>
