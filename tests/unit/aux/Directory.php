<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;

class Directory
{
    public function match(string $directory_path, string $name_pattern): array
    {
        $directory_handle = opendir($directory_path);
        $matched_items = array( );
        while ($directory_item = readdir($directory_handle)):
            if (is_link($directory_item)):
                // for example skip .#*.php temp files from mcedit
                continue 1;
            endif;
            if (1 === preg_match($name_pattern, $directory_item)):
                $item_path = $directory_path . $directory_item;
                array_push($matched_items, $item_path);
            endif;
        endwhile;
        closedir($directory_handle);
        return $matched_items;
    }
}
?>