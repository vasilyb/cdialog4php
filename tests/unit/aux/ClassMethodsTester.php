<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;
use \PHPUnit\Framework\Assert;

trait ClassMethodsTester
{
    private function testClassContainsMethods($classname_or_object,
        ...$wanted_methods_names)
    {
        $existing_methods = get_class_methods($classname_or_object);
        foreach ($wanted_methods_names as $name):
            $method_index = array_search($name, $existing_methods);
            Assert::assertTrue(is_int($method_index),
                "Not found: $name()");
        endforeach;
    }
}
?>
