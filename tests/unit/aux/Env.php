<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Aux;

define('COLLATION', 'LC_ALL');
define('NO_ACS', 'NCURSES_NO_UTF8_ACS');

class Env
{
    public function setNoAcs($no_acs)
    {
        putenv(NO_ACS."=$no_acs");
    }

    public function resetNoAcs()
    {
        $this->setNoAcs('');
    }

    public function setCollation($collation)
    {
        putenv(COLLATION."=$collation");
    }
} // Env
?>
