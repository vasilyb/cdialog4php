<?php
declare(strict_types=1);

namespace CDialog4PHP\UnitTests\Options;
use \CDialog4PHP\Options;

class OptionsImpl extends Options
{
    private $optionName = 'an-option';
    public function setOptionName(string $option_name)
    {
        $this->optionName = $option_name;
    }

    private $optionalOptionName = '';
    private $optionalOptionValue = '';
    public function setOptionalOption(string $name, string $value)
    {
        $this->optionalOptionName = $name;
        $this->optionalOptionValue = $value;
    }

    public function getOptions(): string
    {
        $main_option = $this->formatOption($this->optionName,
            'That\'s all');
        if (0 != strlen($this->optionalOptionName)):
            $optional_option = ' ' . $this->formatOption(
                $this->optionalOptionName, $this->optionalOptionValue);
        else:
            $optional_option = '';
        endif;
        return $main_option . $optional_option;
    }
}
?>
