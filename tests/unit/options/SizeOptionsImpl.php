<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Options;
use \CDialog4PHP\{Options, SizeOptions};

class SizeOptionsImpl extends Options
{
    use SizeOptions;

    function getOptions(): string
	{
		return implode(' ', array(self::getHeight(), self::getWidth()));
	}
}
?>

