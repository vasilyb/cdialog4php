<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests\Options;
use \CDialog4PHP\BoxOptions;

class BoxOptionsImpl extends BoxOptions
{
    private $hello = '';
    public function setHello(string $hello)
    {
        $this->hello = $hello;
    }
    public function getHello(): string
    {
        return $this->hello;
    }

    public function getOptions(): string
    {
        $options = '--version';

        $hello = $this->getHello();
        if (0 <> strlen($hello)):
            $options .= " --and-hello '$hello'";
        endif;

        return $options;
    }
}
?>
