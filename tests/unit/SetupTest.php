<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;

class SetupImpl extends \CDialog4PHP\Setup
{
    private $log;

    public function getLog()
    {
        return $this->log;
    }

    public function setUp()
    {
        $this->log = 'setUp';
    }

    public function setDown()
    {
        $this->log = 'setDown';
    }
}

class SetupTest extends \Codeception\Test\Unit
{
    private $setup_instance;

    protected function _before()
    {
        $this->setup_instance = new SetupImpl();
    }

    protected function _after()
    {
    }

    // tests
    public function testSetMethodsExist()
    {
        $this->setup_instance->setUp();
        $this->assertEquals('setUp', $this->setup_instance->getLog());

        $this->setup_instance->setDown();
        $this->assertEquals('setDown', $this->setup_instance->getLog());
    }
}
