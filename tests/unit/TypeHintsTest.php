<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \Codeception\Util\Debug;
use \CDialog4PHP\UnitTests\Aux\Directory;

class TypeHintsTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testStrictTypesDeclaration()
    {
        $directory = new Directory;
        $php_pattern = '/\.php$/';
        $source_files = $directory->match(__DIR__ . '/../../src/', $php_pattern);
        $test_files = (new Directory)->match(__DIR__ . '/', $php_pattern);
        $php_files = array_merge($source_files, $test_files);

        $decl_pattern = '/declare\(strict_types=1\);/';
        $chars_count = 30;
        Debug::debug('');
        foreach ($php_files as $fpath):
            Debug::debug('Testing ' . $fpath);
            $file_head = file_get_contents($fpath, FALSE, NULL, 0, $chars_count);
            $expected_match = 1;
            $actual_match = preg_match($decl_pattern, $file_head);
            $msg = "A $decl_pattern isn't found in first $chars_count chars of $fpath.";
            self::assertEquals($expected_match, $actual_match, $msg);
        endforeach;
    }
}
?>
