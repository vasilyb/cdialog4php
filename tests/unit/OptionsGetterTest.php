<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\OptionsGetter;
use \CDialog4PHP\UnitTests\Options\OptionsImpl;

class OptionsGetterTest extends \Codeception\Test\Unit
{
    // tests
    public function testApostropheEscaping()
    {
        $options = new OptionsImpl;
        $getter = new OptionsGetter;
        $expected = "--an-option 'That'\''s all'";
        $actual = $getter->getOptions($options);
        self::assertEquals($expected, $actual);
    }
    public function testGetSeveralOptions()
    {
        $options = new OptionsImpl;
        $options->setOptionName('main-option');
        $options->setOptionalOption('optional-option', 'value');
        $getter = new OptionsGetter;
        $expected =
            "--main-option 'That'\''s all' --optional-option 'value'";
        $actual = $getter->getOptions($options);
        self::assertEquals($expected, $actual);
    }
}

