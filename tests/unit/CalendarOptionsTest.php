<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\UnitTests\SizeOptionsTester;
use \CDialog4PHP\{CalendarOptions, Config};
use \CDialog4PHP\UnitTests\Aux\{
    OptionsTester, ParentClassTester, PropertiesTester};
use \Codeception\Test\Unit;

class CalendarOptionsTest extends Unit
{
    use ParentClassTester;
    use SizeOptionsTester;

    private function createOptions()
    {
        $options = new CalendarOptions;
        $options->setHeight(20);
        $options->setWidth(30);
        return $options;
    }

    // tests
    public function testText()
    {
        $data = array();

        $data['Object'] = self::createOptions();
        $data['Property'] = 'Text';
        $data['Expectation'] = 'CDialog4PHP';

        (new PropertiesTester)->testProperty($data);
    }

    public function testOptions()
    {
        $data = array();

        $data['Options'] = self::createOptions();
        $data['Arguments'] = array('text' => 'Text');
        $data['Expectation'] =
			"--calendar 'Text' 20 30 13 4 2019";

        (new OptionsTester)->testAllOptions($data);
    }

    public function testAncestor()
    {
        self::testParentClass('CDialog4PHP\BoxOptions',
            self::createOptions());
    }

    public function testSizeOptionsTraitUsage()
    {
        self::testSizeOptionsMethodsContained(self::createOptions());
    }
}
?>

