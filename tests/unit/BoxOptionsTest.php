<?php
declare(strict_types=1);
namespace CDialog4PHP\UnitTests;
use \CDialog4PHP\BoxOptions;
use \CDialog4PHP\UnitTests\Aux\ParentClassTester;

class BoxOptionsTest extends \Codeception\Test\Unit
{
    use ParentClassTester;

    // tests
    public function testAncestor()
    {
        self::testParentClass('CDialog4PHP\Options',
			'CDialog4PHP\BoxOptions');
    }
}

