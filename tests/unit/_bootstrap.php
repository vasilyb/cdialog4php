<?php
declare(strict_types=1);

use Codeception\Util\Autoload;

Autoload::addNamespace('CDialog4PHP\UnitTests',
    __DIR__);
Autoload::addNamespace('CDialog4PHP\UnitTests\Aux',
    __DIR__ . '/aux');
Autoload::addNamespace('CDialog4PHP\UnitTests\Options',
    __DIR__ . '/options');

?>
