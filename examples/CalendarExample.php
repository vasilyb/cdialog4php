#!/usr/bin/env php
<?php
require_once __DIR__ . '/../vendor/autoload.php';

use \CDialog4PHP\{CalendarOptions,
	CommonOptions,
	Config,
	Dialog};

$common_options = new CommonOptions;
$common_options->setTitle('Dialog Example');

$box_options = new CalendarOptions;
$box_options->setText('Calendar example');

$dialog_config = new Config($common_options, $box_options);

$calendar = new Dialog($dialog_config);
$calendar->open();
?>
