#!/usr/bin/env php
<?php
require_once __DIR__ . '/../vendor/autoload.php';

use \CDialog4PHP\{BuildListOptions,
	CommonOptions,
	Config,
	Dialog};

$common_options = new CommonOptions;
$common_options->setTitle('Dialog example');

$build_list_options = new BuildListOptions;
$build_list_options->setText('Build list example');

$config = new Config($common_options, $build_list_options);
$build_list = new Dialog($config);
$build_list->open();
?>
