<?php
declare(strict_types=1);

namespace CDialog4PHP;

class Utf8Setup extends Setup {
	private $no_utf8_acs; // previous value

	const NO_UTF8_ACS = 'NCURSES_NO_UTF8_ACS';

	private function detectUtf8(): bool {
		$lc_all = getenv('LC_ALL');
		if ( empty($lc_all) ):
			$lc_all = '';
		endif; // LC_ALL is NULL
		$utf8 = preg_match('/[a-z]{2}_[A-Z]{2}\\.UTF-8/', $lc_all);
		return 1 === $utf8;
	} // detectUtf8()

	public function setUp() {
		$utf8 = $this->detectUtf8();
		$this->no_utf8_acs = getenv(Utf8Setup::NO_UTF8_ACS);
		$new_acs = $utf8? '1': '';
		putenv(Utf8Setup::NO_UTF8_ACS."=$new_acs");
	} // setUp()

	public function setDown() {
		putenv(Utf8Setup::NO_UTF8_ACS.'='.$this->no_utf8_acs);
	} // setDown()
} // class
?>
