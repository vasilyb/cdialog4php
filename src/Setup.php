<?php
declare(strict_types=1);

namespace CDialog4PHP;

abstract class Setup {
	abstract public function setUp();
	abstract public function setDown();
} // class
?>
