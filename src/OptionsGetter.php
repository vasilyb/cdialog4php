<?php
declare(strict_types=1);

namespace CDialog4PHP;

class OptionsGetter {
	public function getOptions(Options $options): string {
		return $options->getOptions();
	} // getOptions()
} // class OptionsGetter
?>

