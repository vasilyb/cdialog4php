<?php
declare(strict_types=1);

namespace CDialog4PHP;

class Entries extends \ArrayObject {
	public function append($value) {
		static $expected_class_name = 'CDialog4PHP\Entry';
		if ($expected_class_name == get_class($value)):
			parent::append($value);
		endif; // wrong class
	} // append()
} // Entries
?>

