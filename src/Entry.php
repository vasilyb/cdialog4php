<?php
declare(strict_types=1);

namespace CDialog4PHP;

use \CDialog4PHP\EntryState;

class Entry {
    private $tag = '';
    public function setTag(string $tag) {
        $this->tag = $tag;
    } // setTag()
    public function getTag(): string {
        return $this->tag;
    } // getTag()

    private $item = '';
    public function setItem(string $item) {
        $this->item = $item;
    } // setItem()
    public function getItem(): string {
        return $this->item;
    } // getItem()

    // todo replace string with EntryState
    private $state = EntryState::OFF;
    public function setState(string $state = EntryState::OFF) {
        // available states are listed in EntryState
        if (! in_array($state, array(EntryState::OFF, EntryState::ON))):
            throw new \Exception(__METHOD__ . '() - $state must be "' .
                EntryState::OFF . '" or "' . EntryState::ON . '".');
        endif;
        $this->state = $state;
    } // setState()
    public function getState(): string {
        // returns one of the state listed in EntryState
        return $this->state;
    } // getState()
} // Entry
?>
