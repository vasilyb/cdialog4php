<?php
declare(strict_types=1);

namespace CDialog4PHP;

class EntryState {
    const OFF = 'off';

    const ON = 'on';
} // EntryState
?>
