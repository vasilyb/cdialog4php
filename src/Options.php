<?php
declare(strict_types=1);

namespace CDialog4PHP;

abstract class Options {
	protected function formatOption(string $param = '', string $arg):
		string {
		$param_named = strlen($param) > 0;
		$double_dash = $param_named? '--': '';
		$white_space = $param_named? ' ': '';
		$prepared_arg = escapeshellarg($arg);
		return $double_dash . $param . $white_space . $prepared_arg;
	} // formatOption()

	public abstract function getOptions(): string;
} // class Options
?>

