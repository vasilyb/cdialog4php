<?php
declare(strict_types=1);

namespace CDialog4PHP;

class CommonOptions extends Options {
    private $title = '';
    public function setTitle(string $title) {
        $this->title = $title;
    } // setTitle()
    public function getTitle(): string {
        return $this->title;
    } // getTitle()

    public function getOptions(): string {
        $title = $this->formatOption('title', $this->getTitle());
        return "$title";
    } // getOptions()
} // class CommonOptions
?>

