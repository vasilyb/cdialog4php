<?php
declare(strict_types=1);

namespace CDialog4PHP;

/**
 * This is an empty abstract class for
 * specific dialog boxes information to be
 * consumed by cdialog.
 */
abstract class BoxOptions extends Options {
} // class BoxOptions
?>

