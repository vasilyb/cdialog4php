<?php
declare(strict_types=1);

namespace CDialog4PHP;

class CalendarOptions extends BoxOptions {
    use SizeOptions;
    private $text = '';
    public function setText(string $text) {
        $this->text = $text;
    } // setText()
    public function getText(): string {
        return $this->text;
    } // getText()

    public function getOptions(): string {
        $text = $this->getText();
        $size = $this->getSize();
        return "--calendar '$text' $size 13 4 2019";
    } // getOptions()
} // class CalendarOptions
?>
