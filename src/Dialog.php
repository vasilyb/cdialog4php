<?php
declare(strict_types=1);

namespace CDialog4PHP;

class Dialog {
	private $setups;
	private $config;

	public function __construct(Config $config) {
		// setting up terminal
		$utf8setup = new Utf8Setup;
		$this->setups = array( $utf8setup );

		// configuring a dialog
		$this->config = $config;
	} // __construct()

	private function exploitSetups(string $direction) {
		$setups_count = is_array($this->setups)?
			count($this->setups): 0;
		if ($setups_count !== 0):
			$method = "set$direction";
			for ($i = 0; $i < $setups_count; $i++):
				$this->setups[$i]->$method();
			endfor; // each setup
		endif; // setups is an array
	} // exploitSetups()

	private function beginOpen() {
		$this->exploitSetups('Up');
	} // beginOpen()

	private function getOptions(): string {
		return $this->config->getOptions();
	} // getOptions()

	private function performOpen() {
		$options = $this->getOptions();
		passthru("cdialog $options");
	} // performOpen()

	private function finishOpen() {
		$this->exploitSetups('Down');
	} // finishOpen()

	public function open() {
		$this->beginOpen();
		$this->performOpen();
		$this->finishOpen();
	} // open()
} // class
?>
