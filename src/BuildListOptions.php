<?php
declare(strict_types=1);

namespace CDialog4PHP;

class BuildListOptions extends BoxOptions {
    use SizeOptions;

    private $text = '';
    public function setText(string $text) {
        $this->text = $text;
    } // setText()
    public function getText(): string {
        return $this->text;
    } // getText()

    private $listHeight = Size::MINIMUM;
    public function setListHeight(int $height = Size::MINIMUM) {
        $this->listHeight = $height;
    } // setListHeight()
    public function getListHeight(): int {
        return $this->listHeight;
    } // getListHeight()

    // todo convert type to Entries extends \ArrayObject
    private $entries = array();

    public function addEntry(
        string $tag,
        string $item,
        string $state = EntryState::OFF)
    {
        $entry = new Entry;

        $entry->setTag($tag);
        $entry->setItem($item);
        $entry->setState($state);

        $this->entries[] = $entry;
    } // addEntry()

    public function getEntries() {
        return $this->entries;
    } // getEntries()

    public function getOptions(): string {
        $text = $this->getText();
        $size = $this->getSize();
        $list_height = $this->getListHeight();
        return
            "--buildlist '$text' $size $list_height fbsd FreeBSD on";
    } // getOptions()
} // class BuildListOptions
?>
