<?php
declare(strict_types=1);

namespace CDialog4PHP;

class Size {
    // If a height or a width is set to Size::SCREEN, cdialog uses
    // the screen's size
    const SCREEN = -1;
    
    // If a height or a width is set to Size::MINIMUM, cdialog uses
    // minumum size for widget to display the prompt and data
    const MINIMUM = 0;
} // Size
?>

