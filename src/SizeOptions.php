<?php
declare(strict_types=1);

namespace CDialog4PHP;

trait SizeOptions {
    // special values are Size::SCREEN and Size::MINUMUM
    private $height = Size::MINIMUM;

    // special values are Size::SCREEN and Size::MINUMUM
    private $width = Size::MINIMUM;

    public function setHeight(int $height = Size::MINIMUM) {
		$this->height = $height;
    } // setHeight()

    public function getHeight(): int {
		return $this->height;
    } // getHeight()

    public function setWidth(int $width = Size::MINIMUM) {
		$this->width = $width;
    } // setWidth()

    public function getWidth(): int {
		return $this->width;
    } // getWidth()

    public function getSize(): string {
        $height = self::getHeight();
        $width = self::getWidth();
        return "{$height} {$width}";
    } // getSize()
} // SizeOptions
?>

