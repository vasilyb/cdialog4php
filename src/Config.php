<?php
declare(strict_types=1);

namespace CDialog4PHP;

class Config extends Options {
    private $commonOptions;
    private $boxOptions;

    public function __construct(CommonOptions $commonOptions,
        BoxOptions $boxOptions) {
        $this->commonOptions = $commonOptions;
        $this->boxOptions = $boxOptions;
    } // __construct()

    public function getOptions(): string {
        $opts_objs = array($this->commonOptions,
            $this->boxOptions);
        $opts_getter = new OptionsGetter;
        $get_opts = array($opts_getter, 'getOptions');
        $opts_strs = array_map($get_opts, $opts_objs);
        return implode(' ', $opts_strs);
    } // getOptions()

    public function editCommonOptions(): CommonOptions {
        return $this->commonOptions;
    } // editCommonOptions()

    public function editBoxOptions(): BoxOptions {
        return $this->boxOptions;
    } // editBoxOptions()
} // class Config
?>

